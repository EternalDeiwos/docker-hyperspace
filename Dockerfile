FROM node:16

ENV LANG=en_US.utf8

RUN npm install -g hyperspace

COPY docker-entrypoint.sh /usr/local/bin
USER node:node
ENTRYPOINT [ "docker-entrypoint.sh" ]
STOPSIGNAL SIGINT

CMD [ "hyperspace", "--memory-only", "--port", "30300" ]